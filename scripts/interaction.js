define(function() {
  var Interaction = {
    handleStageMouseMove: function(renderer, clock, schedule, event) {
      var moment = clock.getRadiansMoment(renderer.getRadians(event.stageX, event.stageY));
      var rawMoment = clock.getRadiansMomentRaw(renderer.getRadians(event.stageX, event.stageY));
      var nextMoment = clock.getNextRadiansMoment(renderer.getRadians(event.stageX, event.stageY));
      renderer.setInfoElementString(moment.format('hh:mm:ss'));
      renderer.positionInfoElement(event.stageX, event.stageY);
      renderer.setTrackerEnd(clock.getMomentRadians(nextMoment));
      renderer.setMiddleText(schedule.getAppointmentTitleAt(rawMoment));
      if(renderer.trackerEnabled()) {
        var apptRadians = renderer.getTrackerRadians();
        var apptStart = clock.getNextRadiansMoment(apptRadians.start);
        var apptEnd = clock.getNextRadiansMoment(apptRadians.end);
        // While event creation is in progress, info element should show the event duration
        renderer.setInfoElementString(apptStart.format('hh:mm:ss') + ' to ' + apptEnd.format('hh:mm:ss'));
      } else {
        // Show the current moment
        renderer.setInfoElementString(moment.format('hh:mm:ss'));
        // If event selection is not in progress, just set the start wherever we are
        renderer.setTrackerStart(clock.getMomentRadians(moment));
      }
    },
    handleStageMouseDown: function(renderer, clock, schedule, event) {
      if(renderer.trackerEnabled()) {
        renderer.disableTracker();
        var apptRadians = renderer.getTrackerRadians();
        var apptStart = clock.getNextRadiansMoment(apptRadians.start);
        var apptEnd = clock.getNextRadiansMoment(apptRadians.end);
        schedule.addAppointment(apptStart, apptEnd, '');
      } else {
        renderer.enableTracker();
        // Don't change the line below - see bug #1
        renderer.setTrackerStart(clock.getMomentRadians(clock.getRadiansMoment(renderer.getRadians(event.stageX, event.stageY))));
      }
    },
    handleClockMouseOver: function(renderer) {
      renderer.showHoverElements();
    },
    handleClockMouseOut: function(renderer) {
      renderer.hideHoverElements();
    }
  };

  return Interaction;
});