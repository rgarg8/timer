define(['text!plugins/google/gcalendar.html'], function(templateHtml) {
  var GCal = Class.extend({
    oauthUri: 'https://accounts.google.com/o/oauth2/v2/auth?scope=https://www.googleapis.com/auth/calendar.readonly&state=&redirect_uri=http://app-routinemaker.rhcloud.com&response_type=token&client_id=1092463556798-m8tc8vg7jfhmrh6c4udh21osm11sfmgu.apps.googleusercontent.com',
    init: function(clock, schedule) {
      this.clock = clock;
      this.schedule = schedule;
      rivets.components['gcalendar'] = {
        template: this.template.bind(this),
        initialize: this.setup.bind(this)
      }
    },
    setup: function(el, data) {
      var keyAndHash = location.hash.match(new RegExp('access_token=([^&]*)'));
      if (keyAndHash) {
        this.accessToken = keyAndHash[1];
      }
      this.fetchCalendars(null, this);
      return this;
    },
    template: function () {
      return templateHtml;
    },
    fetchCalendars: function(event, scope) {
      fetch('https://www.googleapis.com/calendar/v3/users/me/calendarList', {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer '+scope.accessToken
        }
      }).then(function(response) {
        response.json().then(function(json){
          scope.calendars = json.items;
          $('select').material_select();
        });
      });
    },
    fetchEvents: function(event, scope) {
      var calendarId = $('select').val();
      fetch('https://www.googleapis.com/calendar/v3/calendars/'+calendarId+'/events?timeMin='+scope.clock.start.toISOString()+'&timeMax='+scope.clock.end.toISOString(), {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer '+scope.accessToken
        }
      }).then(function(response) {
        response.json().then(function(json){
          json.items.forEach(function(event) {
            try {
              scope.schedule.addAppointment(moment(event.start.dateTime), moment(event.end.dateTime), event.summary);
            } catch(err) {
              console.log(err, event);
            }
          });
        });
      });
    },
    syncEvents: function(event, scope) {
      // Will sync existing events in the schedule to Google calendar, IF
      // the summary doesn't match - uses the currently selected calendar
      var calendarId = $('select').val();
      fetch('https://www.googleapis.com/calendar/v3/calendars/'+calendarId+'/events?timeMin='+scope.clock.start.toISOString()+'&timeMax='+scope.clock.end.toISOString(), {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer '+scope.accessToken
        }
      }).then(function(response) {
        response.json().then(function(json){
          var existingEvents = {};
          var eventsToPush = [];
          json.items.forEach(function(event) {
            if(event.hasOwnProperty('summary')) {
              existingEvents[event.summary] = event;
            }
          });
          scope.schedule.getAppointments().forEach(function(appt) {
            if(!existingEvents.hasOwnProperty(appt.title)) {
              eventsToPush.push(appt);
            }
          });
          console.log('These events would be sync\'d: ', eventsToPush);
        });
      });
    }
  });

  return GCal;
});
