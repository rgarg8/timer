define(['plugins/google/gcalendar'], function(GCal) {
  var Plugins = Class.extend({
    init: function(clock, schedule) {
      this.gcalendar = new GCal(clock, schedule);
    }
  });

  return Plugins;
});