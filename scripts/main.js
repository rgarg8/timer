define(['clock', 'schedule', 'renderer', 'chimer', 'saver', 'loader', 'plugins'], function(Clock, Schedule, Renderer, Chimer, Saver, Loader, Plugins) {
  var loader = new Loader();
  var savedClock = loader.getClock();
  var savedSchedule = loader.getSchedule();

  var defaultStart = {
    hour: 9,
    minute: 0,
    second: 0
  };
  
  var defaultEnd = {
    hour: 17,
    minute: 0,
    second: 0
  };

  var clock;
  if(savedClock) {
    clock = new Clock(savedClock);
  } else {
    clock = new Clock(moment().set(defaultStart), moment().set(defaultEnd));
  }

  var schedule;
  if(savedSchedule) {
    schedule = new Schedule(savedSchedule);
  } else {
    schedule = new Schedule();
  }

  var saver = new Saver(clock, schedule);
  var chimer = new Chimer(clock, schedule);
  var renderer = new Renderer(clock, schedule);
  renderer.beginRendering(500,500);

  // Initialize plugins
  var plugins = new Plugins(clock, schedule);

  // Bind HTML (only need to do it once)
  var container = document.getElementById('rivetsContainer');
  rivets.bind(container, {
    clock: clock,
    setToNow: clock.setToNow.bind(clock),
    save: saver.save.bind(saver),
    appointments: schedule.getAppointments(),
    remove: schedule.removeAppointment.bind(schedule),
    add: schedule.addAppointment.bind(schedule, moment(), moment().add(5,'s'), '')
  });

  // Fix
  Materialize.updateTextFields();
});
