define(function() {
  var ClockEvent = Class.extend({
    init: function(tag, start, callback) {
      this.tag = tag;
      this.start = start;
      this.callback = callback;
      this.fired = false;
    },
    checkAndFire: function(current) {
      if(!this.fired && current.isAfter(this.start)) {
        this.callback();
        this.fired = true;
      }
    }
  });

  return ClockEvent;
});