define(['color'], function(Color) {
  var Appointment = Class.extend({
    init: function(start, end, title) {
      if(start.hasOwnProperty('serial')) {
        var saved = start;
        this.start = moment(saved.start);
        this.end = moment(saved.end);
        this.title = saved.title;
        this.color = saved.color;
      } else {
        this.start = start;
        this.end = end;
        this.title = title;
        this.color = Color.generateRandomPastel();
      }
    },
    serialize: function() {
      return {
        start: this.start,
        end: this.end,
        title: this.title,
        color: this.color,
        serial: true
      };
    }
  });

  var Schedule = Class.extend({
    init: function(saved) {
      this.appointments = [];
      this.modCallbacks = [];
      if(typeof(saved) === 'object') {
        saved.forEach(function(serialized) {
          this.addAppointment(serialized);
        }.bind(this));
      }
    },
    serialize: function() {
      var serialized = [];
      this.appointments.forEach(function(appointment) {
        serialized.push(appointment.serialize());
      });
      return serialized;
    },
    addAppointment: function(start, end, title) {
      this.appointments.push(new Appointment(start, end, title));
      this.onModify();
    },
    // This method is a bit strange due to the way rivet calls methods
    removeAppointment: function(event, appointment) {
      this.appointments.splice(appointment.index, 1);
      this.onModify();
    },
    getAppointments: function() {
      return this.appointments;
    },
    getAppointmentTitleAt: function(givenTime) {
      var title = '';
      this.appointments.forEach(function(appointment) {
        if(givenTime >= appointment.start && givenTime < appointment.end) {
          title = appointment.title;
        }
      });
      return title;
    },
    onModify: function(callback) {
      // Dual purpose - register if arg, call otherwise
      if(typeof(callback) !== 'undefined') {
        this.modCallbacks.push(callback);
      } else {
        this.modCallbacks.forEach(function(callback) {
          callback(this.appointments);
        }.bind(this));
      }
    }
  });

  return Schedule;
});