rivets.formatters.date = {
  publish: function(value) {
    var potential = moment(value);
    if(potential.isValid()) {
      return potential;
    } else {
      return value;
    }
  },
  read: function(value) {
    if(typeof(value) === 'string') {
      return value;
    } else {
      return value.format('MM/DD/YYYY hh:mm:ss a');
    }
  }
};

rivets.formatters.number = {
  publish: function(value) {
    return parseInt(value, 10);
  },
  read: function(value) {
    return value;
  }
};