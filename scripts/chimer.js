define(['clockevent'], function(ClockEvent) {
  var Chimer = Class.extend({
    init: function(clock, schedule) {
      this.audio = document.getElementsByTagName('audio')[0];
      this.clock = clock;
      this.schedule = schedule;
      schedule.onModify(this.calibrate.bind(this));
      this.calibrate(schedule.getAppointments());
    },
    calibrate: function(appointments) {
      this.clock.removeClockEventsByTagName('chime');
      appointments.forEach(function(appointment) {
        if(appointment.start.isAfter(this.clock.current)) {
          this.clock.addClockEvent(new ClockEvent('chime', appointment.start, this.chime.bind(this)));
        }
      }.bind(this));
    },
    chime: function() {
      this.audio.play();
    }
  });

  return Chimer;
});