define(function() {
  var Clock = Class.extend({
    init: function(start, end) {
      this.clockEvents = {};
      // Check whether we should deserialize or init
      if(start.hasOwnProperty('serial')) {
        var saved = start;
        this.start = moment(saved.start);
        this.end = moment(saved.end);
        this.current = moment(saved.current);
        this.increment = saved.increment;
        this.cursorSize = saved.cursorSize;
        this.cursorUnit = saved.cursorUnit;
      } else {
        this.start = start;
        this.end = end;
        this.current = moment();
        this.increment = 's';
        this.cursorSize = 30;
        this.cursorUnit = 'm';
      }
      setInterval(this.tick.bind(this), 1000);
    },
    serialize: function() {
      return {
        start: this.start,
        end: this.end,
        current: this.current,
        increment: this.increment,
        cursorSize: this.cursorSize,
        cursorUnit: this.cursorUnit,
        serial: true
      };
    },
    addClockEvent: function(clockEvent) {
      if(!this.clockEvents.hasOwnProperty(clockEvent.tag)) {
        this.clockEvents[clockEvent.tag] = [];
      }
      this.clockEvents[clockEvent.tag].push(clockEvent);
    },
    removeClockEventsByTagName: function(tagName) {
      if(this.clockEvents.hasOwnProperty(tagName)) {
        this.clockEvents[tagName] = [];
      }
    },
    processClockEvents: function(current) {
      Object.keys(this.clockEvents).forEach(function(tagName) {
        this.clockEvents[tagName].forEach(function(clockEvent) {
          clockEvent.checkAndFire(current);
        }.bind(this));
      }.bind(this));
    },
    tick: function() {
      this.current.add(1, this.increment);
      // Don't allow ticking past the end of time
      if(this.current.isAfter(this.end)) {
        this.current = moment(this.end);
      }
      this.processClockEvents(this.current);
    },
    setToNow: function() {
      this.current = moment();
    },
    getHandRadians: function() {
      // What is the range?
      var range = this.start.diff(this.end);
      var now = this.start.diff(this.current);
      var degrees = (now/range) * 360;
      return degrees * (Math.PI/180);
    },
    getMomentRadians: function(moment) {
      var range = this.start.diff(this.end);
      var now = this.start.diff(moment);
      var degrees = (now/range) * 360;
      return degrees * (Math.PI/180);
    },
    getRadiansMomentRaw: function(radians) {
      var range = this.start.diff(this.end);
      var fraction = radians/(Math.PI*2);
      var diff = -1 * fraction * range;
      return moment(this.start).add(diff, 'ms');
    },
    getRadiansMoment: function(radians) {
      var duration = moment.duration(this.cursorSize, this.cursorUnit);
      return this.roundMoment(this.getRadiansMomentRaw(radians) - duration, duration);
    },
    getNextRadiansMoment: function(radians) {
      var duration = moment.duration(this.cursorSize, this.cursorUnit);
      return this.roundMoment(this.getRadiansMomentRaw(radians), duration);
    },
    roundMoment: function (origMoment, duration) {
      return moment(Math.ceil((+origMoment) / (+duration)) * (+duration));
    }
  });

  return Clock;
});