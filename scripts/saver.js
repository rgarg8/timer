define(function() {
  var Saver = Class.extend({
    init: function(clock, schedule) {
      this.clock = clock;
      this.schedule = schedule;
    },
    save: function() {
      alert('clock='+JSON.stringify(this.clock.serialize())+'&schedule='+JSON.stringify(this.schedule.serialize()));
    }
  });

  return Saver;
});