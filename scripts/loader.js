define(function() {
  var Loader = Class.extend({
    getHashValue: function(key) {
      var value = '';
      var keyAndHash;
      key = (typeof key === 'string') ? key : '';
      keyAndHash = location.hash.match(new RegExp(key + '=([^&]*)'));
      if (keyAndHash) {
          value = keyAndHash[1];
      }  
      return value;
    },
    getClock: function() {
      var serialized = this.getHashValue('clock');
      if(serialized.length > 0) {
        return JSON.parse(serialized);
      } else {
        return false;
      }
    },
    getSchedule: function() {
      var serialized = this.getHashValue('schedule');
      if(serialized.length > 0) {
        return JSON.parse(serialized);
      } else {
        return false;
      }
    }
  });

  return Loader;
});