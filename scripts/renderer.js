define(['interaction'], function(Interaction) {
  var Renderer = Class.extend({
    init: function(clock, schedule) {
      this.shouldShowHover = false;
      this.shouldShowTracker = false;
      this.trackStart = 0;
      this.trackEnd = 0;
      this.clock = clock;
      this.schedule = schedule;
      this.stage = new createjs.Stage('canvas');
      this.stage.enableMouseOver();
      this.stage.on('stagemousemove', Interaction.handleStageMouseMove.bind(Interaction, this, this.clock, this.schedule));
      this.stage.on('stagemousedown', Interaction.handleStageMouseDown.bind(Interaction, this, this.clock, this.schedule));
      this.middleText = '';
    },
    beginRendering: function(width, height) {
      this.width = width;
      this.height = height;
      this.clockCenter = width/2;
      this.clockRadius = width/2;
      setInterval(this.render.bind(this), 1000/30);
    },
    render: function() {
      this.stage.removeAllChildren();
      var outerCircle = new createjs.Shape();
      outerCircle.graphics.beginFill('#ccc')
        .drawCircle(this.clockCenter, this.clockCenter, this.clockRadius);
      outerCircle.alpha = 0.2;
      outerCircle.on('mouseover', Interaction.handleClockMouseOver.bind(Interaction, this));
      outerCircle.on('mouseout', Interaction.handleClockMouseOut.bind(Interaction, this));
      this.stage.addChild(outerCircle);
      // Draw tracker
      if(this.shouldShowHover) {
        var tracker = new createjs.Shape();
        tracker.graphics.beginFill('#000')
          .moveTo(this.clockCenter, this.clockCenter)
          .arc(this.clockCenter, this.clockCenter, this.clockRadius, this.trackStart - Math.PI/2, this.trackEnd - Math.PI/2);
        if (this.shouldShowTracker) {
          tracker.alpha = 0.5;
        } else {
          tracker.alpha = 0.2;
        }
      } else {
        this.middleText = '';
      }
      this.stage.addChild(tracker);
      // Draw all appointments
      this.schedule.getAppointments().forEach(function(appointment) {
        var appt = new createjs.Shape();
        appt.graphics.beginFill(appointment.color)
          .moveTo(this.clockCenter,this.clockCenter)
          .arc(this.clockCenter, this.clockCenter, this.clockRadius, this.clock.getMomentRadians(appointment.start) - Math.PI/2, this.clock.getMomentRadians(appointment.end) - Math.PI/2);
        appt.alpha = 0.5;
        this.stage.addChild(appt);
      }.bind(this));
      var hand = new createjs.Shape();
      hand.graphics.beginFill('#f00')
        .moveTo(this.clockCenter,this.clockCenter)
        .arc(this.clockCenter, this.clockCenter, this.clockRadius, this.clock.getHandRadians() - Math.PI/2, this.clock.getHandRadians() + Math.PI/360 - Math.PI/2);
      this.stage.addChild(hand);
      var innerCircle = new createjs.Shape();
      innerCircle.graphics.beginFill('#fff')
        .drawCircle(this.clockCenter, this.clockCenter, this.clockRadius-100);
      this.stage.addChild(innerCircle);
      var textToShow = this.middleText;
      var textColor = '#ff7700';
      if(textToShow === '') {
        textToShow = this.schedule.getAppointmentTitleAt(this.clock.current);
        textColor = '#000000';
      }
      var middleTextElement = new createjs.Text(textToShow, '20px Raleway', textColor);
      var middleTextBounds = middleTextElement.getBounds();
      if(middleTextBounds != null) {
        middleTextElement.x = this.clockCenter - middleTextBounds.width / 2;
        middleTextElement.y = this.clockCenter - middleTextBounds.height / 2;
      }
      this.stage.addChild(middleTextElement);
      this.stage.update();

      // Deal with showing or hiding info
      var infoEl = document.getElementById('info');
      infoEl.style.display = this.shouldShowHover ? 'block' : 'none';
    },
    positionInfoElement: function(stageX, stageY) {
      var infoEl = document.getElementById('info');
      var radians = this.getRadians(stageX, stageY) - Math.PI/2;
      // Convert radians back into cartesians on the edge of the circle
      var relX = (Math.cos(radians) * this.clockRadius);
      var relY = (Math.sin(radians) * this.clockRadius);
      var x = this.clockCenter + relX;
      var y = this.clockCenter + relY;
      // Modify the relative coordinates to be the offset for the info element (keep it off-radius a little)
      relX = relX/this.clockRadius * infoEl.offsetWidth;
      relY = relY/this.clockRadius * infoEl.offsetHeight;
      // Get the canvas location relative to the document
      var documentPos = document.body.getBoundingClientRect();
      var canvasPos = document.getElementById('canvas').getBoundingClientRect();
      var canvasOffset = {
        left: canvasPos.left - documentPos.left,
        top: canvasPos.top - documentPos.top
      };
      // Position the info element's center, not its top left corner, use the offsetWidth and offsetHeight
      infoEl.style.left = canvasOffset.left + x - (infoEl.offsetWidth/2) + relX + 'px';
      infoEl.style.top = canvasOffset.top + y - (infoEl.offsetHeight/2) + relY + 'px';
    },
    setTrackerStart: function(radians) {
      this.trackStart = radians;
    },
    setTrackerEnd: function(radians) {
      this.trackEnd = radians;
    },
    enableTracker: function() {
      this.shouldShowTracker = true;
    },
    disableTracker: function() {
      this.shouldShowTracker = false;
    },
    trackerEnabled: function() {
      return this.shouldShowTracker;
    },
    showHoverElements: function() {
      this.shouldShowHover = true;
    },
    hideHoverElements: function() {
      this.shouldShowHover = false;
    },
    setInfoElementString: function(str) {
      var infoEl = document.getElementById('info');
      infoEl.textContent = str;
    },
    setMiddleText: function(text) {
      this.middleText = text;
    },
    getRadians: function(stageX, stageY) {
      // Convert cursor position to radians based on what we know
      var radians = Math.atan2(stageY - this.clockCenter, stageX - this.clockCenter) + Math.PI/2;
      // The adjustment below isn't needed, but feels nice (-180,180) to (0,360)
      return (radians < 0) ? radians + 2*Math.PI : radians;
    },
    getTrackerRadians: function() {
      return {
        start: this.trackStart,
        end: this.trackEnd
      }
    }
  });

  return Renderer;
});